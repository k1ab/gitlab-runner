# Gitlab runner

Docker based runner with Docker executer managed by docker-compose.

- [Gitlab runner](#gitlab-runner)
  - [Register](#register)
  - [Start](#start)
  - [Stop](#stop)
  - [Container access](#container-access)

## Register

This is general Gitlab runner setup so in order to start using it you need to register it first.

1. From `sample.env` create `.env` file.
2. Obtain GitLab [registration token](https://docs.gitlab.com/runner/register/)
3. Fill out `.env` file with relevant information.
4. Create empty `config/config.toml` file:

   ```bash
   touch config/config.toml
   ```

5. [Start](#start) the runner.
6. [Enter](#container-access) the container.
7. Run through registration process:

   ```bash
   gitlab-runner register
   ```

   The command will update `config.toml`. It is a local file and is not tracked by the repository.
   Configuration options can be found in [this doc](https://docs.gitlab.com/runner/configuration/).

## Start

```bash
docker-compose up -d
```

## Stop

```bash
docker-compose down
```

## Container access

```bash
docker exec -it gitlab_runner /bin/bash
```
